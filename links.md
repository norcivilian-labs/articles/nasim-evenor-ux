# links
## search bar
## big refactor for branch main
  - [Refactor mobile PR](https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/17)
  - [Refactor mobile](https://gitlab.com/norcivilian-labs/evenor/-/issues/47)

    В общих чертах старые комп перенести в новые папки и проверить все ли верно компилируются. Не переписываться компоненты, не менять функционал. Просто перемещаем в другие папки и удаляем старое. Старое перемещаем в папку trash. Все устарелое буду удалять и копировать в trash
В старом у нас 3 компонента: header, overview, profile.
внутри header - кнопки навигации, радио кнопки для смены кнопки overview и filter
внутри overview много разных overview
внутри профиля 2 разных профиля - эдит и view
внутри их свой маленький header и компоненты с данными.

После рефактора у нас будет 3 больших комп
overview, profileView, profileEdit
внутри overview будет один overview, так же filter и кнопки навигации
внутри pofileview будут поля с данными, кнопки навигации для профиля
внутри pofileedit будут поля с данными, кнопки навигации для профиля
Не переименовываем папки, создаем только новые и пытаемся их правильно соединить
на рефакторе появляется кнопка плюс и нужно будет перемести на верх в header
на каждый шаг рефактора писать тест, code path. сравнить со старым

Store не меняем.Src/main : находятся папки electron. переходят в папку src/electron
Src/server - удаляем.
Src/lib/latex - удаляем(код для генерации pdf)
Src/lib/dispensers - перемещаем в src/layout/profile_view/components/dispenser
Src/lib/api -> Src/api
Меняем папки конфигурации webpack
пример C:\Users\Accountant\Desktop\web-work\projects\evenor\webpack.main.config.js это папки для electron
внутри него меняем путь папок. 6 строчка, 36 строчка

Моя задача:

сделать backup evenor
merge request новый

Задачи

надо написать от руки структуру всех папок новых.
расписать порядок шагов рефактора. с чего надо начать
сделать backup evenor
в каждом компоненте должен быть файл css. На поломанный css пишем комментарий в файл css конкретного компонента
добавить linter, добавить стили в vs-code, табы, пробелы, кавычки одинарные/двойные.
в penpot warframe сделать детальнее и красивее
название новую гит ветку: refactor-mobile


changed title from refactor to refactor mobile

here's structure of folders
https://hackmd.io/@fetsorn/HkmUL-ou6


 move src/main to src-electron, fix webpack config and imports

 move src/server to src-server, fix imports

 delete src/lib/latex

 move src/lib/api to src/api, fix imports and lib alias

 move src/lib/dispensers to src/layout/profile_view/components/dispenser, fix imports

 delete src/lib

 at this point src/ should have only renderer/

 move src/renderer/i18n to src/i18n, fix import in app.jsx

 move src/renderer/store and src/renderer/components to src/store and src/components, fix @ alias, fix imports

 at this point src/renderer should have only layout/ and app.jsx

 move src/renderer/app.jsx to src/app.jsx, same for index.html and index.css, fix webpack config, import root from ./renderer/layout/root.jsx

 move src/renderer/layout/root.jsx to src/layout/root.jsx, import components from ../renderer/layout/..., update import in app.jsx

 delete src/renderer/layout/footer, fix imports

 at this point src/renderer should have only layout/ with header/, overview/ and profile/

 copy code from src/renderer/layout/profile/profile.jsx to src/layout/root.jsx, import from ../renderer/layout/profile/components

 move src/renderer/layout/profile/components/profile_single_edit to src/layout/profile_edit, fix imports

 same for profile_single_view

 delete src/renderer/profile

 delete src/renderer/header/components/header_overview_radio, fix imports, fix store, fix src/renderer/overview/overview.jsx to only show itinerary

 move src/renderer/layout/overview/overview.jsx to src/layout/overview/overview.jsx, import from ../../renderer/layout/overview/components, fix imports in src/layout/root.jsx

 move src/renderer/layout/header/components/header_filter to src/layout/overview/components/overview_filter, add import and code to overview.jsx

 move "home" and "settings" buttons from src/renderer/layout/header/header.jsx to src/layout/overview/overview.jsx, also move functions used in onClick to overview.jsx

 delete src/renderer/layout/header

 at this point the only old folder left is src/renderer/layout/overview

 move src/renderer/layout/overview/components/overview_itinerary to src/layout/overview, combine code with overview.jsx, rename component, fix imports

 delete src/renderer

 fix css so that OverviewItinerary is under OverviewFilter



## search bar refactor

  - [add + button to search bar](https://gitlab.com/norcivilian-labs/evenor/-/work_items/36)
    copy button from https://gitlab.com/norcivilian-labs/evenor/-/tree/main/src/renderer/components/dropdown?ref_type=heads

  - ["Debug query list"](https://gitlab.com/norcivilian-labs/evenor/-/work_items/37)

    entries are not updated when you type
    filterSearchListNew/onChange(value) -> store/onQueryAdd(field,value) -> store/queries -> filterSearchListNew/input/value -> filterSearchListNew/onChange(value)

  - [implement menu items array](https://gitlab.com/norcivilian-labs/evenor/-/work_items/38)
      / schema {category, local_path, reponame}
      // Obj.keys(schema) ["category", "local_path", "reponame"]
      // a = Object.keys(schema).map(key=>({label: key})) '[{"label": "category"},{"label": "local_path"},{"label": "reponame"}]'
      // [ { onClick, label: "category" }, {onClick, label: "local_path"}, {onClick, label:"reponame"}]
      // onClick - добавляет запрос, при добавлении запрос должен перейти в filter_query_list, при этом удалив из menuItems

  - [delete query from btn plus, when you create new query](https://gitlab.com/norcivilian-labs/evenor/-/work_items/39)

  - [filter overview on query change](https://gitlab.com/norcivilian-labs/evenor/-/work_items/40)

    фильтрация элементов при введении любой буквы
    сразу при обновлении

    современно
    везде на новых сайтах

    случайно можно ввести что-то не то и не заметить

    но есть одна буква которая соответствует

    плюс несколько букв если на ф одно слово


      enter filter query value
      component filter_search_bar onQeryInput
      in component header_filter
      function onQueryInput in onQueryInput
      use setQueryValue filter_search_bar
      pass queryValue call funct onQueryAdd inside obj store
      in file filter_slice use onQueryAdd
      Add new field queryValue to var queries.
      this var queries and it inside store
      perhaps in func onQueryAdd
      perhaps in component header there is useEffect which depends on queries and repoUUID
      call in store funct onQueries or onChangeBase
      call in store funct updatOverview
      changes var overview in store
      ???
      url updated
  


  - [update URL on filter change](https://gitlab.com/norcivilian-labs/evenor/-/work_items/43)
    Header: take queries from store and transform searchParams
    Header: searchParams trasform to string url
    Header: call navigate to write string to browser
    Root:take location from useLocation(url) call initialize
    Overview_slice: func initialize transform var queries to paramsToQueries
    Overview_slice: initialize names search
    Overview_slice: search transform searchParams
    Overview_slice: searchParams transform to queries
    Overview_slice: queries saves in store
    - [update URL on filter change PR](https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/15)

## search bar/input break
https://gitlab.com/norcivilian-labs/evenor/-/issues/45
resolved in 
https://gitlab.com/norcivilian-labs/evenor/-/commit/c29fbbadc7741af59a911b6a34735c4f6ac98221

The steps should happen in this order
      we want to search for apples. we press "find" in filter
      set 1st closeHandler
      start 1st search
      add entries about apples to store.overview variable
      now we want to search for tomatoes. we press "find" in filter again
      call 1st closeHandler
      finish 1st search
      set store.overview variable to empty
      set 2nd closeHandler
      start 2nd search
      add entries about tomatoes to store.overview variable
## url doesn't update

https://gitlab.com/norcivilian-labs/evenor/-/issues/71

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/27

need add #/ to filter url
we set URL somewhere in the store with history.replaceState
instead of "/reponame" we should set "/#/reponame"

  
## profile
## hide schema
issue:
https://gitlab.com/norcivilian-labs/evenor/-/issues/62
resolved in PR:
https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/21

hide schema view from profile_view

hide schema from the sidebar, only show it in the "settings" screen
show schema view only if repoUUID!=="root". this will hide schema on the first screen but will still show it in the settings.
where is the component that shows "Schema of the repo"?
Right now the "Schema of the repo" is in the ViewField components, inside the switch case branchType==="array". If the branch is an array, ViewField shows the description of this branch. This branch is called "schema" and is stored in the api/schema.js file, in the variable schemaSchema.
Right now ViewField works for any array. And if we want to hide the "schema" array, we will have to add an exception to ViewField for the "schema" branch. This is not great code.
So, we need to add a if inside the "case 'array'" in ViewField which checks that

      repoUUID!=="root"

      branch !== "schema"

This will look something like

 return { (repoUUID !== "root" && branch !== "schema") && (
        <div>
          {!isOpen ? (
            <div>
       ...
      }

## add label to entry uuid in profile_view, hide under spoiler
issue
https://gitlab.com/norcivilian-labs/evenor/-/issues/55

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/20

right now uuid is shown in profile_view as symbols and numbers. Instead, it should be shown as a spoiler and a text saying "unique code of entry".
where uuid is shown in components?
how we show spoilers?
how can we add spoilers to uuid?
Right now the "uuid" is at the top of the ViewField component.
So, we need to add a spoiler around the uuid, add a label to i18n, and show it near the spoiler. russian label is "код записи", english is "entry code"

## update i18n in InputUpload
https://gitlab.com/norcivilian-labs/evenor/-/issues/58

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/22

Component is called "InputUpload" L66

"выбрать файл" instead of "Загрузить"

the i18n label path is line.button.upload

## hide array dropdown from profile_edit

https://gitlab.com/norcivilian-labs/evenor/-/issues/53

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/24

right now we show options for array items just like objects and other unique entities, but it hasn't found use and is confusing. we should hide this
this code in input_array.jsx should be commented for now

      { options.length > 0 && (
        <select
          value="default"
          onChange={({ target: { value } }) => {
            onFieldChange(branch, JSON.parse(value));
          }}
        >
          <option hidden disabled value="default">
            {t('line.dropdown.input')}
          </option>

          {options.map((field) => (
            <option key={crypto.getRandomValues(new Uint32Array(10)).join('')} value={JSON.stringify(field)}>
              {JSON.stringify(field)}
            </option>
          ))}
        </select>
      )}

      # add list of options and default values to input fields in ProfileEdit and FilterQueryList

      https://gitlab.com/norcivilian-labs/evenor/-/issues/29
      
      https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/26

      right now input fields are empty when user opens profile_edit for the first time. Input fields should show option list of all possible values.
if the field is in the filter, filter value should come first in the list of options
if there are five names in the database, then when we add a new field “name” to the event, a list with five options appears; if the filter contains a search for the name “bob”, then the new field should show list of options where the word “bob” is first

Before the search bar refactor we had these "options" in the FilterSearchBar component. src/renderer/layout/header/components/header_filter/components/filter_search_bar/filter_search_bar.jsx
The code should be something like this

...
const values = await api.queryOptions(branch);
...
        <input list={branch} ... />
        <datalist id={branch}>
          {values.map((value) => (
            <option value={value} />
          ))}
        </datalist>


## replace "add array item" dropdown with a button "add item element"

https://gitlab.com/norcivilian-labs/evenor/-/issues/60

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/28

right now when we want to add an array item we show a dropdown that says "add field", and the dropdown has only one field. instead we should show a button that says "add {array item description}".
the dropdown is an InputDropdown copmponent in src/renderer/layout/profile/components/profile_single_edit/components/input_array/input_array.jsx
InputDropdown takes a variable leaves, this variable is an array of all branches that have the array as trunk. but array usually has only one leaf. So if the array leaves has only one element, we show a button instead of a dropdown.
the onClick function in the button should call onFieldAddArrayItem with the single element of the leaves array
the button should have a label from i18n. in case of "files" the label on the button should be - "выбрать из существующих объектов", "выбрать из загруженных файлов" or "загрузить новый файл"

## hide "Upload" button from profile_edit when file is uploaded

https://gitlab.com/norcivilian-labs/evenor/-/issues/59

user won't see "upload" button after they uploaded one file
user must add another digital asset element if they want to upload another file
right now the upload button is in src/renderer/layout/profile/components/profile_single_edit/components/input_upload/input_upload.jsx
we need to add an if statement that checks if file was uploaded. we check it like this

entry[filenameBranch] && entry[filehashBranch]

so the code will be something like this:

      {__BUILD_MODE__ === 'electron' ? (
        <Button type="button" onClick={() => onUpload()}>
          {t('line.button.upload')}
        </Button>
      ) : (
        <input
          type="file"
          onChange={(e) => onUpload(e.target.files[0])}
        />
      )}

## Add "Add event button" to List overview

https://gitlab.com/norcivilian-labs/evenor/-/issues/24

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/29

Now there is no way to add an event in the List overview. We need to add a way to do it.
We need to make it clear for a new user how to add first event. Possible ways:\


        
      tooltip on the add button?\

        
      
“+” button and text “Create your first project/event” (better for the elderly)

## overview
## bug with blue button that opens editing project

https://gitlab.com/norcivilian-labs/evenor/-/issues/73

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/30

open first page app click to list(check mark) then click to blue button and open edit(pencil). After shows bug

## add "edit" button to each item in the list overview

https://gitlab.com/norcivilian-labs/evenor/-/issues/51

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/30

the same as "edit" button in profile_edit
each item in list overview should have button to open project, button to open profile_edit and button to open profile_view

## doesn't show list

https://gitlab.com/norcivilian-labs/evenor/-/issues/72

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/31

radio button doesn't selected when opens first page of app
when you open project and choose list of projects(check mark) it shows, however you click Home it shows list of projects again, but it has to show timeline

## show groupBy in the label of ListingItem

https://gitlab.com/norcivilian-labs/evenor/-/issues/25

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/32

Right now the ListingItem is hardcoded to show the reponame field. Instead it should show the "groupBy" field.
If the grouping field changes, the headings in the list must also change accordingly.
For example, if events were grouped by saydate, the headers should include the entry date.
If the grouping basis changes to actdate, then the event date should appear in the headings.
for example if groupBy is reponame, show project name for each project. but if groupBy is category, show category for each project

## Tooltip with short event description

https://gitlab.com/norcivilian-labs/evenor/-/issues/35

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/33

show a tooltip with text from the datum field on the hover.
The text in the field by which events are grouped may be too long. Should trim the text, either depending on the screen size (the text should fit on the screen in one line) or by a fixed number of characters.
Right now in the timeline an entry is defined in WaypointEntries. renderer/layout/overview/components/overview_itinerary/components/waypoint_entries/waypoint_entries.jsx

// turn the entry object into text
// {_:"datum", UUID: "221312301840918", reponame: "Milan", category: "msic", files: {} }
// "Name of project: Milan, Category: msic"
// "Название проекта: Milan, Категория: msic"
function listing2text(listing) {
  const keys = Object.keys(listing)

  // map over keys of listing object, append key and value to one string
  const textPairs = keys.map((key) => {
    const description = schema[key].description[lang];

    const value = listing[key];

    const textPair = `${description}: ${value}`

    return textPair
  })
  
  // return string of text
  const text = textPairs.join(", ")
}

...
const tooltip = listing2text(listing)
...
<div class={styles.entry}>
  <button
    title={tooltip}
    ...
  >
    {index + 1}
  </button>
</div>

## Rename .groupBy to .sortBy

https://gitlab.com/norcivilian-labs/evenor/-/issues/26

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/33

Entries in the list should be sorted in alphabetical order by the field by which they are grouped (and which, accordingly, is in the header).
This is already how groupBy works in itinerary and list.
rename .groupBy to .sortBy or .orderBy everywhere
originally "group by" meant "sort all entries by this field and group entries with the same field as a step on the timeline". But some views don't have such "grouping" feature. Also "groupBy" conflicts with SQL aggregate function's GROUP BY which is has a completely different behavior.
So this should just be called "sortBy" or "orderBy", to resemble SQL's ORDER BY

## rename variable overview and component overview differently

https://gitlab.com/norcivilian-labs/evenor/-/issues/41

https://gitlab.com/norcivilian-labs/evenor/-/merge_requests/34

the variable in store should be called something else, for example "entries" or "dataset". The component name should stay the same

src\renderer\store\overview_slice.js use variable overview
233 line we change overview to records to line 254
line 302 and line 377 change overview
src\renderer\store\entry_slice.js line 228 change overview
line 244
src\renderer\layout\overview\components\overview_book\overview_book.jsx
src\renderer\layout\overview\components\overview_chat\overview_chat.jsx line 17
src\renderer\layout\overview\components\overview_itinerary\overview_itinerary.jsx line 18
src\renderer\layout\overview\components\overview_listing\overview_listing.jsx line 23



